﻿namespace Blackjack.ViewModels.Statistic
{
    public class GetRoundsViewModel
    {
        public int Start { get; set; }

        public int Count { get; set; }

        public string PlayerName { get; set; }
    }
}