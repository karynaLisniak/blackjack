﻿using System.Collections.Generic;
using Blackjack.ViewModels.Round;

namespace Blackjack.ViewModels.Statistic
{
    public class StatisticsRoundsViewModel
    {
        public IList<RoundViewModel> Rounds { get; set; }

        public int CountRounds { get; set; }
    }
}