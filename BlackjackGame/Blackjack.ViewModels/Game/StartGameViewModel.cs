﻿namespace Blackjack.ViewModels.Game
{
    public class StartGameViewModel
    {
        public string Name { get; set; }

        public int Cash { get; set; }
    }
}