﻿namespace Blackjack.ViewModels.Game
{
    public class DealViewModel
    {
        public long GameId { get; set; }

        public long PlayerId { get; set; }

        public int CountBots { get; set; }

        public int Rate { get; set; }
    }
}