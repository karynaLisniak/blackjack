﻿namespace Blackjack.ViewModels.Game
{
    public class GameViewModel
    {
        public long GameId { get; set; }

        public long PlayerId { get; set; }
    }
}