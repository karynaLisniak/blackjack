﻿using System.Collections.Generic;

namespace Blackjack.ViewModels.Round
{
    public class PlayerViewModel
    {
        public long Id { get; set; }

        public string Role { get; set; }

        public string Name { get; set; }

        public int Cash { get; set; }

        public int Rate { get; set; }

        public int Score { get; set; }

        public List<CardViewModel> Cards { get; set; }

        public PlayerViewModel()
        {
            Cards = new List<CardViewModel>();
        }
    }
}
