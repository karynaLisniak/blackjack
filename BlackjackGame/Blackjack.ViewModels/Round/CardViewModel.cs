﻿namespace Blackjack.ViewModels.Round
{
    public class CardViewModel
    {
        public long Id { get; set; }

        public string Suit { get; set; }

        public string Rank { get; set; }

        public int Points { get; set; }
    }
}
