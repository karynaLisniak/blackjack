﻿using Blackjack.BusinessLogic.Interfaces;
using Blackjack.ViewModels.Game;
using Blackjack.ViewModels.Round;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Blackjack.Web.Controllers.Api
{
    [Route("api/game")]
    public class GameApiController : Controller
    {
        private readonly IGameService _gameProvider;

        public GameApiController(IGameService gameService)
        {
            _gameProvider = gameService;
        }

        [HttpPost("start")]
        public async Task<IActionResult> StartGame([FromBody] StartGameViewModel requestModel)
        {
            GameViewModel responseStartGameModel = await _gameProvider.StartGame(requestModel);
            return Ok(responseStartGameModel);
        }

        [HttpPost("deal")]
        public async Task<IActionResult> Deal([FromBody] DealViewModel requestModel)
        {
            RoundViewModel responseModel = await _gameProvider.Deal(requestModel);
            return Ok(responseModel);
        }

        [HttpPost("hit")]
        public async Task<IActionResult> Hit([FromBody] HitViewModel requestModel)
        {
            RoundViewModel responseHitModel = await _gameProvider.Hit(requestModel);
            if (responseHitModel == null)
            {
                return BadRequest();
            }

            return Ok(responseHitModel);
        }

        [HttpPost("stand")]
        public async Task<IActionResult> Stand([FromBody] StandViewModel requestModel)
        {
            RoundViewModel responseModel = await _gameProvider.Stand(requestModel);
            if (responseModel == null)
            {
                return BadRequest();
            }

            return Ok(responseModel);
        }

        [HttpPost("getPlayerState")]
        public async Task<IActionResult> GetRoundState([FromBody] GetPlayerStateViewModel requestModel)
        {
            PlayerViewModel responseModel = await _gameProvider.GetPlayerStateModel(requestModel);
            return Ok(responseModel);
        }
    }
}