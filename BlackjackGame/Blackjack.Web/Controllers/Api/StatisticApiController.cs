﻿using Blackjack.BusinessLogic.Interfaces;
using Blackjack.ViewModels.Statistic;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Blackjack.ViewModels.Round;

namespace Blackjack.Web.Controllers.Api
{
    [Route("api/statistic")]
    public class StatisticApiController : Controller
    {
        private readonly IStatisticService _statisticService;

        public StatisticApiController(IStatisticService statisticService)
        {
            _statisticService = statisticService;
        }

        [HttpPost("getRound")]
        public async Task<IActionResult> GetRound([FromBody] GetRoundViewModel requestModel)
        {
            RoundViewModel round = await _statisticService.GetRoundById(requestModel);
            return Ok(round);
        }

        [HttpPost("getRounds")]
        public async Task<IActionResult> GetRounds([FromBody] GetRoundsViewModel requestModel)
        {
            StatisticsRoundsViewModel responseModel = await _statisticService.GetRounds(requestModel);
            return Ok(responseModel);
        }
    }
}