﻿using AutoMapper;
using Blackjack.BusinessLogic.AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace Blackjack.Web.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddMapper(this IServiceCollection services)
        {
            MapperConfiguration mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MapperProfile()); });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}