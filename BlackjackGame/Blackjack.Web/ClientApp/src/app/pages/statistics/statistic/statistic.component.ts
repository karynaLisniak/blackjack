import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';

import { StatisticService } from '../../../../shared/services/statistic.service';
import { Round } from '../../../../shared/models/round';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.css']
})
export class StatisticComponent implements OnInit {

  private gridView: GridDataResult;
  private data: Round[];
  private total: number;
  private pageSize: number = 5;
  private skip: number = 0;
  private sort: SortDescriptor[] = [{
    field: 'player',
    dir: 'asc'
  }];

  private playerName: string;
  private selectedData: Round;

  constructor(private modalService: NgbModal, private statisticService: StatisticService) {

  }

  ngOnInit() {

  }

  public pageChange(event: PageChangeEvent) {
    this.skip = event.skip;
    this.getRounds(this.playerName);
  }

  public sortChange(sort: SortDescriptor[]) {
    this.sort = sort;
    this.loadItems();
  }

  public gridSelectionChange(selection) {
    this.selectedData = selection.selectedRows[0].dataItem;
  }

  public getRounds(playerName: string = "") {
    this.playerName = playerName;

    this.statisticService.getRounds(this.skip, this.pageSize, playerName)
      .subscribe(response => {
        this.data = response.rounds;
        this.total = response.countRounds;

        this.loadItems();
      },
        error => console.log(error)
      );
  }

  public getRoundById() {
    this.statisticService.getRound(this.selectedData.roundId)
      .subscribe(roundData => {
        const modalRef = this.modalService.open(ModalComponent);
        modalRef.componentInstance.round = roundData;
      },
        error => console.log(error)
      );
  }

  private loadItems() {
    this.gridView = {
      data: orderBy(this.data, this.sort),
      total: this.total
    }
  }

}
