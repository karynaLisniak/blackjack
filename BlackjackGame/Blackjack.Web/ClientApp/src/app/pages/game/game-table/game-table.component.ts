import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { Round } from '../../../../shared/models/round';
import { Bot } from '../../../../shared/models/bot';
import { GameService } from '../../../../shared/services/game.service';

@Component({
  selector: 'app-game-table',
  templateUrl: './game-table.component.html',
  styleUrls: ['./game-table.component.css']
})

export class GameTableComponent implements OnInit {

  private round = new Round();
  private isRoundResult = false;

  roundForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private gameService: GameService) {

  }

  ngOnInit() {
    this.initForm();
    this.getPlayerState();
  }

  initForm() {
    this.roundForm = this.formBuilder.group({
      rate: [null, [
        Validators.required,
        Validators.pattern(/\b[1-9]*\b/)
      ]],
      countBots: [null, [
        Validators.pattern(/\b[0-9]+\d{0,1}\b/)
      ]]
    });
  }

  public getPlayerState() {
    var playerId = this.getPlayerId();

    this.gameService.getPlayerState(playerId)
      .subscribe(player => {
        this.round.player = player;
      },
        error => console.log(error)
      );
  }

  public startRound() {
    var rate = this.roundForm.value.rate;
    var countBots = this.roundForm.value.countBots;

    this.isRoundResult = false;

    this.gameService.startRound(this.getGameId(), this.getPlayerId(), rate, countBots)
      .subscribe(roundData => {
        localStorage.setItem('roundId', roundData.roundId.toString());

        this.round = roundData;

        if (roundData.status != 'NotFinished') {
          this.round.status = roundData.status;
          this.isRoundResult = true;
        }
      },
        error => console.log(error)
      );
  }

  public hit() {
    var playerId = this.getPlayerId();
    var roundId = this.getRoundId();

    this.gameService.hit(playerId, roundId)
      .subscribe(roundData => {
        this.addPlayersCards(roundData);

        if (roundData.status != 'NotFinished') {
          this.round.status = roundData.status;
          this.isRoundResult = true;
        }
      },
        error => console.log(error)
      );
  }

  public stand() {
    var playerId = this.getPlayerId();
    var roundId = this.getRoundId();

    this.gameService.stand(playerId, roundId)
      .subscribe(roundData => {
        this.addPlayersCards(roundData);

        this.round.status = roundData.status;
        this.isRoundResult = true;
      },
        error => console.log(error)
      );
  }

  private getPlayerId() {
    var playerId = localStorage.getItem('playerId');
    return playerId;
  }

  private getGameId() {
    var gameId = localStorage.getItem('gameId');
    return gameId;
  }

  private getRoundId() {
    var roundId = localStorage.getItem('roundId');
    return roundId;
  }

  private addPlayersCards(round: Round) {
    this.round.player.score = round.player.score;
    this.round.player.cards = this.round.player.cards.concat(round.player.cards);

    this.round.dealer.score = round.dealer.score;
    this.round.dealer.cards = this.round.dealer.cards.concat(round.dealer.cards);

    for (var index = 0; index < round.bots.length; index++) {
      var bot = this.findBot(round.bots[index].name);
      bot.score = round.bots[index].score;
      bot.cards = bot.cards.concat(round.bots[index].cards);
    }
  }

  private findBot(botName: string): Bot {
    return this.round.bots.find((bot) => {
      if (bot.name == botName) {
        return true;
      }
      return false;
    })
  }
}

