import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { GameRoutingModule } from './game-routing.module'

import { GameTableComponent } from './game-table/game-table.component';

@NgModule({
  declarations: [
    GameTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    GameRoutingModule
  ],
  exports: [
    GameTableComponent
  ],
  providers: [HttpClientModule]
})
export class GameModule { }
