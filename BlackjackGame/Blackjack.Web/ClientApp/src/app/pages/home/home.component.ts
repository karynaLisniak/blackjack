import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Player } from '../../../shared/models/player';
import { GameService } from '../../../shared/services/game.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  playerForm: FormGroup;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private gameService: GameService) {

  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.playerForm = this.formBuilder.group({
      playerName: [null, [
        Validators.required
      ]],
      cash: [null]
    });
  }

  public startGame() {
    var player = this.getPlayerModel();

    this.gameService.startGame(player)
      .subscribe(startGameModel => {
          localStorage.setItem('playerId', startGameModel.playerId.toString());
          localStorage.setItem('gameId', startGameModel.gameId.toString());

          this.router.navigate(['/gameTable']);
        },
        error => console.log(error)
      );
  }

  public viewStatistics() {
    this.router.navigate(['/statistic']);
  }

  private getPlayerModel() {
    var player = new Player();
    player.name = this.playerForm.value.playerName;
    player.cash = this.playerForm.value.cash;

    return player;
  }

}
