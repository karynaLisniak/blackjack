import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  { 
    path: '', 
    redirectTo: '/home', pathMatch: 'full' 
  },
  { 
    path: 'home', 
    component: HomeComponent 
  },
  { 
    path: 'gameTable', 
    loadChildren: './pages/game/game.module#GameModule' 
  },
  {
    path: 'statistic',
    loadChildren: './pages/statistics/statistic.module#StatisticModule'
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {

}
