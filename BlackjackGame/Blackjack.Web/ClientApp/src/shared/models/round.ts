import { Player } from "./player";
import { Bot } from "./bot";

export class Round {
  roundId: number;
  player: Player;
  bots: Bot[];
  dealer: Bot;
  countBots: number;
  status: string;

  public constructor() {
    this.player = new Player();
    this.bots = [];
    this.dealer = new Bot();
    this.countBots = 0;
  }
}
