export class Card {
  id: number;
  suit: string;
  rank: string;
  points: number;
}
