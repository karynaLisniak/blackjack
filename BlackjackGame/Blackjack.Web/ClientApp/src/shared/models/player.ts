import { Card } from "./card";

export class Player {
  id: number;
  name: string;
  cash: number;
  isPlayed: boolean;
  rate: number;
  cards: Card[];
  score: number;
  status: string;

  public constructor() {
    this.score = 0;
    this.cards = [];
  }
}
