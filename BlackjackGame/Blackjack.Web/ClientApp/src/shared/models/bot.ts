import { Card } from "./card";

export class Bot {
  id: number;
  name: string;
  cards: Card[];
  score: number;

  public constructor() {
    this.score = 0;
    this.cards = [];
  }
}
