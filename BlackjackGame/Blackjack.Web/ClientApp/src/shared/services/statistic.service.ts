import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Round } from '../models/round';
import { Rounds } from '../models/rounds';

@Injectable({
  providedIn: 'root'
})
export class StatisticService {

  private url = '/api/statistic';

  constructor(private http: HttpClient) {

  }

  getRounds(start: number, count: number, playerName: string = ""): Observable<Rounds> {
    return this.http
      .post<Rounds>((`${this.url}/getRounds`), { count, start, playerName });
  }

  getRound(roundId: number): Observable<Round> {
    return this.http
      .post<Round>((`${this.url}/getRound`), { roundId });
  }
}
