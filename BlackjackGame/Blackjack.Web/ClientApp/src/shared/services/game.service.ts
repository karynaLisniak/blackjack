import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Player } from '../models/player';
import { Round } from '../models/round';
import { Game } from '../models/game'

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private url = '/api/game';

  constructor(private http: HttpClient) {

  }

  startGame(player: Player): Observable<Game> {
    return this.http
      .post<Game>((`${this.url}/start`), player);
  }

  startRound(
    gameId: string,
    playerId: string,
    rate: number,
    countBots: number): Observable<Round> {
    return this.http
      .post<Round>((`${this.url}/deal`), { gameId, playerId, countBots, rate });
  }

  hit(playerId: string, roundId: string): Observable<Round> {
    return this.http
      .post<Round>((`${this.url}/hit`), { playerId, roundId });
  }

  stand(playerId: string, roundId: string): Observable<Round> {
    return this.http
      .post<Round>((`${this.url}/stand`), { playerId, roundId });
  }

  getPlayerState(playerId: string): Observable<Player> {
    return this.http
      .post<Player>((`${this.url}/getPlayerState`), { playerId });
  }

}
