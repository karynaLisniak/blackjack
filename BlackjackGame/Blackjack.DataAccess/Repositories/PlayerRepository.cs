﻿using Blackjack.DataAccess.Interfaces;
using Blackjack.Entities.Entities;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Blackjack.Entities.Enums;
using Z.Dapper.Plus;

namespace Blackjack.DataAccess.Repositories
{
    public class PlayerRepository : IPlayerRepository
    {
        private readonly string _connectionString;

        public PlayerRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<Player> Create(Player player)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                player.Id = await dbConnection.InsertAsync(player);
            }
            return player;
        }

        public async Task<List<Player>> Create(IList<Player> players)
        {
            DapperPlusManager.Entity<Player>().Table("Players").Identity(x => x.Id);

            using (TransactionScope transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (IDbConnection dbConnection = new SqlConnection(_connectionString))
                {
                    await dbConnection.BulkActionAsync(conn => conn.BulkInsert(players));

                    transaction.Complete();
                }
            }
            return players.ToList();
        }

        public async Task<Player> GetById(long id)
        {
            Player player;

            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                player = await connection.GetAsync<Player>(id);
            }
            return player;
        }


        public async Task<Player> GetByNameAndRole(string playerName, PlayerRole role = PlayerRole.User)
        {
            string sqlQuery = "SELECT * FROM Players WHERE Name = @playerName AND Role = @role";

            Player player;

            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                player = await dbConnection.QueryFirstOrDefaultAsync<Player>(sqlQuery, new { playerName, role });
            }
            return player;
        }

        public async Task Update(Player player)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                await connection.UpdateAsync(player);
            }
        }
    }
}