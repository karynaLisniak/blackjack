﻿using Blackjack.DataAccess.Interfaces;
using Blackjack.Entities.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Blackjack.DataAccess.Repositories
{
    public class CardRepository : ICardRepository
    {
        private readonly string _connectionString;

        public CardRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<List<Card>> GetFreeCards(long roundId)
        {
            string sqlQuery = @"SELECT * FROM Cards C 
                                WHERE C.Id NOT IN (SELECT PlayerRound.CardId from Rounds Round 
		                            JOIN PlayerRounds PlayerRound on PlayerRound.RoundId = Round.Id
		                            WHERE Round.Id = @roundId)";

            List<Card> cards;
            Dictionary<long, Card> dictCards = new Dictionary<long, Card>();

            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                IEnumerable<Card> result = await dbConnection.QueryAsync<Card>(sqlQuery, new { roundId });
                cards = result.Distinct().ToList();
            }
            return cards;
        }
    }
}