﻿using Blackjack.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blackjack.DataAccess.Interfaces
{
    public interface ICardRepository
    {
        Task<List<Card>> GetFreeCards(long roundId);
    }
}