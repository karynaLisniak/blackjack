﻿using System.Threading.Tasks;
using Blackjack.Entities.Entities;

namespace Blackjack.DataAccess.Interfaces
{
    public interface IGameRepository
    {
        Task<Game> Create(Game game);
    }
}