﻿using Blackjack.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blackjack.DataAccess.Interfaces
{
    public interface IPlayerRoundRepository
    {
        Task<List<PlayerRound>> Create(IList<PlayerRound> playerRounds);
    }
}