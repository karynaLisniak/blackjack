﻿namespace Blackjack.Entities.Enums
{
    public enum PlayerRole
    {
        User = 0,
        Bot = 1, 
        Dealer = 2
    }
}