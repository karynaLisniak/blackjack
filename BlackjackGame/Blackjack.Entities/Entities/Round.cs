﻿using Blackjack.Entities.Bases;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using Blackjack.Entities.Enums;

namespace Blackjack.Entities.Entities
{
    public class Round : BaseEntity
    {
        public long GameId { get; set; }

        public int Rate { get; set; }

        public int Cash { get; set; }

        public PlayerStatus Status { get; set; }

        [Write(false)]
        public virtual ICollection<PlayerRound> PlayerRounds { get; set; }
    }
}
