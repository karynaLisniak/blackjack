﻿using Blackjack.Entities.Bases;
using Blackjack.Entities.Enums;

namespace Blackjack.Entities.Entities
{
    public class Card  : BaseEntity
    {
        public Suit Suit { get; set; }

        public Rank Rank { get; set; }

        public int Points { get; set; }
    }
}
