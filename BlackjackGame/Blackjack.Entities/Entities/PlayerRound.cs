﻿using Blackjack.Entities.Bases;
using Dapper.Contrib.Extensions;

namespace Blackjack.Entities.Entities
{
    public class PlayerRound : BaseEntity
    {
        public long PlayerId { get; set; }

        public long RoundId { get; set; }

        public long CardId { get; set; }

        public int CardPoints { get; set; }

        [Write(false)]
        public virtual Player Player { get; set; }

        [Write(false)]
        public virtual Card Card { get; set; }
    }
}
