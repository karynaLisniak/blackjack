﻿using Blackjack.DataAccess.Interfaces;
using Blackjack.Entities.Entities;
using System.Threading.Tasks;
using AutoMapper;
using Blackjack.BusinessLogic.Interfaces;
using Blackjack.ViewModels.Game;

namespace Blackjack.BusinessLogic.Managers
{
    public class GameManager : IGameManager
    {
        private readonly IMapper _mapper;
        private readonly IGameRepository _gameRepository;

        public GameManager(
            IMapper mapper,
            IGameRepository gameRepository)
        {
            _mapper = mapper;
            _gameRepository = gameRepository;
        }

        public async Task<GameViewModel> CreateGame()
        {
            Game game = new Game();
            game = await _gameRepository.Create(game);

            GameViewModel gameModel = _mapper.Map<Game, GameViewModel>(game);
            return gameModel;
        }
    }
}