﻿using AutoMapper;
using Blackjack.BusinessLogic.Interfaces;
using Blackjack.DataAccess.Interfaces;
using Blackjack.Entities.Entities;
using Blackjack.Entities.Enums;
using Blackjack.ViewModels.Round;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blackjack.BusinessLogic.Managers
{
    public class RoundManager : IRoundManager
    {
        private readonly IMapper _mapper;
        private readonly IRoundRepository _roundRepository;
        private readonly IPlayerRepository _playerRepository;

        public RoundManager(
            IMapper mapper,
            IRoundRepository roundRepository,
            IPlayerRepository playerRepository)
        {
            _mapper = mapper;
            _roundRepository = roundRepository;
            _playerRepository = playerRepository;
        }

        public async Task<RoundViewModel> CreateRound(long gameId, int rate)
        {
            Round round = new Round
            {
                GameId = gameId,
                Rate = rate,
                Status = PlayerStatus.NotFinished,
                PlayerRounds = new List<PlayerRound>()
            };
            round = await _roundRepository.Create(round);

            RoundViewModel roundModel = _mapper.Map<Round, RoundViewModel>(round);
            return roundModel;
        }

        public async Task<RoundViewModel> ChangeRoundStatus(RoundViewModel roundModel)
        {
            PlayerViewModel user = roundModel.Player;
            PlayerViewModel dealer = roundModel.Dealer;

            if (user.Score == dealer.Score && user.Score <= Constant.BLACKJACK)
            {
                roundModel.Status = PlayerStatus.Push.ToString();
            }
            if (user.Score > Constant.BLACKJACK && dealer.Score > Constant.BLACKJACK)
            {
                roundModel.Status = PlayerStatus.Draw.ToString();
            }
            if (IsDealerWinner(user.Score, dealer.Score)
                || IsPlayerBust(user.Score, dealer.Score))
            {
                roundModel.Status = PlayerStatus.DealerWon.ToString();
                user.Cash -= user.Rate;
            }
            if (IsPlayerWinner(user.Score, user.Cards.Count, dealer.Score)
                || IsDealerBust(user.Score, dealer.Score, user.Cards.Count))
            {
                roundModel.Status = PlayerStatus.PlayerWon.ToString();
                user.Cash += user.Rate * 2;
            }
            if (user.Score == Constant.BLACKJACK && user.Cards.Count == 2)
            {
                roundModel.Status = PlayerStatus.Blackjack.ToString();
                user.Cash += (int)(user.Rate * 1.5);
            }

            Round round = _mapper.Map<RoundViewModel, Round>(roundModel);
            Player player = _mapper.Map<PlayerViewModel, Player>(user);
            await _roundRepository.Update(round);
            await _playerRepository.Update(player);
            return roundModel;
        }

        public async Task<RoundViewModel> GetRound(long roundId)
        {
            Round round = await _roundRepository.GetById(roundId);

            RoundViewModel roundModel = _mapper.Map<Round, RoundViewModel>(round);
            return roundModel;
        }

        private bool IsDealerWinner(int userScore, int dealerScore)
        {
            return (userScore < dealerScore && dealerScore <= Constant.BLACKJACK);
        }

        private bool IsDealerBust(int userScore, int dealerScore, int userCardsCount)
        {
            return (userScore <= Constant.BLACKJACK && dealerScore > Constant.BLACKJACK && userCardsCount != 2);
        }

        private bool IsPlayerWinner(int userScore, int userCardsCount, int dealerScore)
        {
            return (dealerScore < userScore && userScore <= Constant.BLACKJACK && userCardsCount != 2);
        }

        private bool IsPlayerBust(int userScore, int dealerScore)
        {
            return (dealerScore <= Constant.BLACKJACK && userScore > Constant.BLACKJACK);
        }
    }
}