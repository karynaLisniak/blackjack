﻿using System.Threading.Tasks;
using Blackjack.ViewModels.Round;
using Blackjack.ViewModels.Statistic;

namespace Blackjack.BusinessLogic.Interfaces
{
    public interface IStatisticService
    {
        Task<RoundViewModel> GetRoundById(GetRoundViewModel requestModel);

        Task<StatisticsRoundsViewModel> GetRounds(GetRoundsViewModel requestModel);
    }
}