﻿using Blackjack.ViewModels.Round;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blackjack.BusinessLogic.Interfaces
{
    public interface ICardManager
    {
        Task<List<CardViewModel>> GetFreeCards(long roundId);
    }
}