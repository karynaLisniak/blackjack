﻿using System.Threading.Tasks;
using Blackjack.ViewModels.Game;
using Blackjack.ViewModels.Round;

namespace Blackjack.BusinessLogic.Interfaces
{
    public interface IGameService
    {
        Task<GameViewModel> StartGame(StartGameViewModel requestModel);

        Task<RoundViewModel> Deal(DealViewModel requestModel);

        Task<RoundViewModel> Hit(HitViewModel requestModel);

        Task<RoundViewModel> Stand(StandViewModel requestModel);

        Task<PlayerViewModel> GetPlayerStateModel(GetPlayerStateViewModel requestModel);
    }
}