﻿using Blackjack.ViewModels.Round;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blackjack.BusinessLogic.Interfaces
{
    public interface IPlayerManager
    {
        Task<PlayerViewModel> AuthorizeUser(string playerName, int playerCash);

        Task<RoundViewModel> CreateRoundBots(RoundViewModel roundModel, int countBots);

        Task CreatePlayersRounds(IList<PlayerViewModel> playerModels, long roundId);

        Task<PlayerViewModel> GetPlayerById(long playerId);

        PlayerViewModel GivePlayerCards(PlayerViewModel player, IList<CardViewModel> freeCards, int countCards);

        PlayerViewModel GivePlayerAllCards(PlayerViewModel player, IList<CardViewModel> freeCards);
    }
}