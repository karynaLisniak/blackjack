﻿using Blackjack.ViewModels.Round;
using System.Threading.Tasks;

namespace Blackjack.BusinessLogic.Interfaces
{
    public interface IRoundManager
    {
        Task<RoundViewModel> CreateRound(long gameId, int rate);

        Task<RoundViewModel> ChangeRoundStatus(RoundViewModel round);

        Task<RoundViewModel> GetRound(long roundId);
    }
}