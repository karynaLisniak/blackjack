﻿using Blackjack.ViewModels.Game;
using System.Threading.Tasks;

namespace Blackjack.BusinessLogic.Interfaces
{
    public interface IGameManager
    {
        Task<GameViewModel> CreateGame();
    }
}