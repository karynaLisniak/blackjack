﻿using AutoMapper;
using Blackjack.Entities.Entities;
using Blackjack.Entities.Enums;
using Blackjack.ViewModels.Game;
using Blackjack.ViewModels.Round;
using System.Collections.Generic;
using System.Linq;

namespace Blackjack.BusinessLogic.AutoMapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Round, RoundViewModel>()
                .ForMember(dest => dest.RoundId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status.ToString()))
                .ForMember(dest => dest.Player, src => src.ResolveUsing((round, responseModel, playerModel, context) =>
                {
                    List<PlayerRound> userRounds = round.PlayerRounds
                        .Where(playerRound => playerRound.Player.Role == PlayerRole.User).ToList();
                    PlayerViewModel userModel = new PlayerViewModel();

                    if (userRounds.Count > 0)
                    {
                        userModel = context.Mapper.Map<List<PlayerRound>, PlayerViewModel>(userRounds);
                        userModel.Cash = round.Cash;
                        userModel.Rate = round.Rate;
                    }

                    return userModel;
                }))
                .ForMember(dest => dest.Dealer, src => src.ResolveUsing((round, responseModel, playerModel, context) =>
                {
                    List<PlayerRound> dealerRounds = round.PlayerRounds
                        .Where(playerRound => playerRound.Player.Role == PlayerRole.Dealer).ToList();
                    PlayerViewModel dealerModel = new PlayerViewModel();

                    if (dealerRounds.Count > 0)
                    {
                        dealerModel = context.Mapper.Map<List<PlayerRound>, PlayerViewModel>(dealerRounds);
                        dealerModel.Cash = round.Cash;
                        dealerModel.Rate = round.Rate;
                    }

                    return dealerModel;
                }))
                .ForMember(dest => dest.Bots, src => src.ResolveUsing((round, responseModel, playerModel, context) =>
                {
                    List<PlayerViewModel> botModels = new List<PlayerViewModel>();

                    var botRounds = round.PlayerRounds.Where(playerRound => playerRound.Player.Role == PlayerRole.Bot)
                        .GroupBy(playerRound => playerRound.Player.Id);
                    foreach (var botRound in botRounds)
                    {
                        PlayerViewModel botModel =
                            context.Mapper.Map<List<PlayerRound>, PlayerViewModel>(botRound.ToList());
                        botModels.Add(botModel);
                    }

                    return botModels;
                }));

            CreateMap<RoundViewModel, Round>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.RoundId))
                .ForMember(dest => dest.Rate, opt => opt.MapFrom(src => src.Player.Rate))
                .ForMember(dest => dest.Cash, opt => opt.MapFrom(src => src.Player.Cash))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status));

            CreateMap<List<PlayerRound>, PlayerViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.First().Player.Id))
                .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.First().Player.Role.ToString()))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.First().Player.Name))
                .ForMember(dest => dest.Score, opt => opt.MapFrom(src => src.Sum(playerRound => playerRound.CardPoints)))
                .ForMember(dest => dest.Cards, opt => opt.MapFrom(src =>
                    src.Select(playerRound => playerRound.Card).Where(card => card != null)));

            CreateMap<Player, PlayerViewModel>().ReverseMap();

            CreateMap<Game, GameViewModel>()
                .ForMember(dest => dest.GameId, opt => opt.MapFrom(src => src.Id));

            CreateMap<Card, CardViewModel>()
                .ForMember(dest => dest.Suit, opt => opt.MapFrom(src => src.Suit.ToString()))
                .ForMember(dest => dest.Rank, opt => opt.MapFrom(src => src.Rank.ToString()));
        }
    }
}