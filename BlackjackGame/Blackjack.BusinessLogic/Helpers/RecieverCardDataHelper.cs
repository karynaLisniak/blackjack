﻿using Blackjack.Entities.Enums;
using Blackjack.ViewModels.Round;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Blackjack.BusinessLogic.Helpers
{
    public static class RecieverCardDataHelper
    {
        public static CardViewModel GetFreeCard(IList<CardViewModel> cards)
        {
            int countCards = cards.Count();

            Random random = new Random();
            int index = random.Next(1, countCards);

            CardViewModel card = cards[index];
            cards.RemoveAt(index);

            return card;
        }

        public static int GetCardPoint(CardViewModel card, int score, int winnerScore)
        {
            if (card.Rank == Rank.Ace.ToString() && score >= winnerScore)
            {
                return Constant.ACE_CARD_POINTS;
            }

            return card.Points;
        }
    }
}