﻿namespace Blackjack.BusinessLogic
{
    public static class Constant
    {
        public const int BLACKJACK = 21;
        public  const int BOT_POINT = 19;
        public const int DEALER_POINT = 17;
        
        public const int ACE_CARD_POINTS = 1;
    }
}